const express = require('express');
const bodyParser = require('body-parser');

const PORT = 3000;
const HOST = '0.0.0.0';

const app = express();

// Make sure express will know the request is in JSON format
app.use(bodyParser.json());
// Decode params sent via query string
app.use(bodyParser.urlencoded({ extended: false }));

require('./src/controller/authController')(app);
require('./src/controller/projectController')(app);

app.listen(PORT, HOST);
