const jwt        = require('jsonwebtoken');
const config = require('../config/config');

function generateToken(params = {}) {
    return jwt.sign(params, config.auth.secret, {
        expiresIn: 86400
    });
}

module.exports = generateToken;