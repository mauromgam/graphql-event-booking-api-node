const jwt = require('jsonwebtoken');
const config = require('../config/config');

module.exports = (req, res, next) => {
    const authReader = req.headers.authorization;

    if (!authReader) {
        return res.status(401).send({ error: 'No token provided' });
    }

    const parts = authReader.split(' ');

    if (!parts.length === 2) {
        return res.status(401).send({ error: 'Token error' });
    }

    const [ scheme, token ] = parts;

    if (!/^Bearer$/i.test(scheme)) {
        return res.status(401).send({ error: 'Token malformed' });
    }

    jwt.verify(token, config.auth.secret, (err, decoded) => {
        if (err) {
            return res.status(401).send({ error: 'Token invalid' });
        }

        req.userId    = decoded.id;
        req.userEmail = decoded.email;
        req.userName  = decoded.name;

        return next();
    });
};