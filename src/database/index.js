const mongoose = require('mongoose');
const config = require('../config/config');
const bunyan = require('bunyan');

const log = bunyan.createLogger({name: config.container_name});

mongoose.connect(
    `mongodb://${config.db.host}:${config.db.port}/${config.container_name}`,
    {
        useNewUrlParser: true
    }
)
    .then(() => log.info('MongoDB Connected'))
    .catch(err => log.error(err));
mongoose.Promise = global.Promise;

module.exports = mongoose;